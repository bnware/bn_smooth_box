library bn_smooth_box.component;

import 'package:angular2/angular2.dart';

import 'dart:html';
import 'package:logging/logging.dart';

@Directive(
  selector: "[bn-smooth-box]",
)
class BnSmoothBox implements OnInit {
  static const String PIXEL_DEFAULT = "0px";
  static const String RGBA_DEFAULT = "rgba(0, 0, 0, 0)";
  static const String RGB_DEFAULT = "rgb(0, 0, 0)";
  static const NONE_DEFAULT = "none";

  Logger _log = new Logger("BnSmoothBox");
  ElementRef _node;
  CssStyleDeclaration computedStyle;
  CssStyleDeclaration style;


  bool _useBackground = true;
  bool _usePadding = true;
  bool _useBorderRadius = true;
  bool _useBorder = true;
  bool _useShadow = true;

  ///Constructor
  BnSmoothBox(this._node);

  @Input("transparent")
  set transparent(String value) => _useBackground = (value is String) ? false : true;

  @Input("no-background")
  set noBackground(String value) => _useBackground = (value is String) ? false : true;

  @Input("no-padding")
  set noPadding(String value) => _usePadding = (value is String) ? false : true;

  @Input("no-border-radius")
  set noBorderRadius(String value) => _useBorderRadius = (value is String) ? false : true;

  @Input("no-border")
  set noBorder(String value) => _useBorder = (value is String) ? false : true;

  @Input("no-shadow")
  set noShadow(String value) => _useShadow = (value is String) ? false : true;


  @override
  ngOnInit() {
    computedStyle = (_node.nativeElement as Element).getComputedStyle();
    style = (_node.nativeElement as Element).style;

    _log.finest("border-color-computed: ${computedStyle.borderColor}");
    _log.finest("border-width-computed: ${computedStyle.borderWidth}");
    _log.finest("border-style-computed: ${computedStyle.borderStyle}");
    _log.finest("useBorder: ${_useBorder}");

    style
      ..display = "block"
    //padding
      ..padding = (computedStyle.padding == PIXEL_DEFAULT) ? (_usePadding) ? "0 10px 10px 10px" : "" : ""
    //background
      ..background = (computedStyle.backgroundColor == RGBA_DEFAULT) ? (_useBackground) ? "#f5f5f5" : "" : ""
    //border radius
      ..borderRadius = (computedStyle.borderRadius == PIXEL_DEFAULT) ? (_useBorderRadius) ? "5px" : "" : ""
    //border settings
      ..borderWidth = (computedStyle.borderWidth == PIXEL_DEFAULT) ? (_useBorder) ? "1px" : "" : ""
      ..borderColor = (computedStyle.borderColor == RGB_DEFAULT) ? (_useBorder) ? "#fff" : "" : ""
      ..borderStyle = (computedStyle.borderStyle == NONE_DEFAULT) ? (_useBorder) ? "solid" : "" : ""
    //box shadow
      ..boxShadow = (computedStyle.boxShadow == NONE_DEFAULT) ? (_useShadow) ? "1px 2px 4px rgba(0, 0, 0, .4)" : "" : ""
      ..overflow = "hidden";
  }

}