library app_component;

import 'package:angular2/angular2.dart';
import 'package:bn_smooth_box/bn_smooth_box.dart';

@Component(
    selector: "app-component",
    templateUrl: "app_component.html",
    directives: const [BnSmoothBox]
)
class AppComponent {}