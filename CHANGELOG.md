# Changelog

## 2.0.0
- updated to Angular 2 
- changed it from being a component to a directive 
  (helps better with semantics of the html, like applying bn-smooth-box to 
   header, footer, section, etc.)

## 1.0.5
- fixed errors in attribute processing
- added new attributes with default values to readme

## 1.0.4 
- refactored attributes to be NgAttr with default value handling

## 1.0.3
- changed display from inline-block back to block

## 1.0.2
- fixed padding problems with adding an own attribute for that as well as for background and border-radius

## 1.0.1
- fixed error with whole width box

## 1.0.0
- Initial version
